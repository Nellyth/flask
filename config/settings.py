import os

DEBUG = True

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DB_NAME = ""
DB_URL = ""
USERNAME = ""
PASSWORD = ""
SECRET_KEY = ""

try:
    exec(open(os.path.join(BASE_DIR, 'config\\local_settings.py')).read())
except IOError:
    raise Exception('Error reading local settings')


# Database
SQLALCHEMY_DATABASE_URI = f"postgresql+psycopg2://{USERNAME}:{PASSWORD}@{DB_URL}/{DB_NAME}"
SQLALCHEMY_TRACK_MODIFICATIONS = False
