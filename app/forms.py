from .models import Person, User
from flask_wtf import FlaskForm
from wtforms_alchemy import ModelForm
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired
from wtforms import StringField, IntegerField, SubmitField, PasswordField, BooleanField, ValidationError


class PersonFormModel(ModelForm):
    class Meta:
        model = Person
        fields = ['first_name', 'last_name', 'email', 'phone', 'address']
        # exclude = ['id']


class PersonForm(FlaskForm):
    first_name = StringField('First Name', validators=[DataRequired()])
    last_name = StringField('Last Name', validators=[DataRequired()])
    email = EmailField("Email", [DataRequired()])
    phone = IntegerField('phone', validators=[DataRequired()])
    address = StringField('address', validators=[DataRequired()])


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Recuérdame')
    submit = SubmitField('Login')


class UserModelForm(ModelForm):
    repeat_password = PasswordField('Repeat Password', validators=[DataRequired()])

    class Meta:
        model = User
        fields = ['username', 'email', 'password']

    def validate(self):
        if self.password.data != self.repeat_password.data:
            self.password.errors += (ValidationError("Passwords do not match"),)
            return False
        return super(UserModelForm, self).validate()


class UserForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('email', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    repeat_password = PasswordField('repeat_password', validators=[DataRequired()])
