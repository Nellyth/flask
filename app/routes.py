from test1 import db
from .models import Person, User
from flask import current_app as app
from .forms import PersonForm, LoginForm, UserForm
from flask import render_template, redirect, url_for, request
from flask_login import current_user, login_user, logout_user, login_required
from .views import person_create, person_update, person_detail, person_list, person_delete
from .views import user_create, user_list

app.add_url_rule('/person_view/', view_func=login_required(person_list))
app.add_url_rule('/person_view/create/', view_func=login_required(person_create))
app.add_url_rule('/person_view/<int:pk>/', view_func=login_required(person_detail))
app.add_url_rule('/person_view/update/<int:pk>/', view_func=login_required(person_update))
app.add_url_rule('/person_view/delete/<int:pk>/', view_func=login_required(person_delete))

app.add_url_rule('/user/create/', view_func=login_required(user_create))
app.add_url_rule('/user/', view_func=login_required(user_list))


@app.route("/")
def index():
    return render_template("base.html")


@app.route('/person/', methods=['GET'])
@login_required
def list_person_view():
    """list Person."""
    return render_template(
        'list_person.html',
        persons=Person.query.all(),
        title="List Person"
    )


@app.route('/person_create/', methods=['GET', 'POST'])
@login_required
def create_person_view():
    person = Person()
    form = PersonForm()
    if form.validate_on_submit():
        person.first_name = form.first_name.data
        person.last_name = form.last_name.data
        person.email = form.email.data
        person.phone = form.phone.data
        person.address = form.address.data
        db.session.add(person)
        db.session.commit()
        return redirect(url_for('list_person_view'))
    return render_template('create_person.html', title='Person', form=form)


@app.route('/person/<int:pk>/', methods=['GET'])
@login_required
def get_person_view(pk: int):
    """get a user."""
    person = Person.query.filter_by(id=pk)

    if person:
        form = PersonForm()
        return render_template(
            'create_person_view.html',
            title="Person",
            person=person[0],
            form=form,
            option="get"
        )
    else:
        return 'Error loading #{id}'.format(id=pk)


@app.route('/person/put/<int:pk>/', methods=['GET', 'POST'])
@login_required
def put_person_view(pk: int):
    """get a user."""
    person = Person.query.filter_by(id=pk)
    form = PersonForm()

    if request.method == 'GET' and person:
        return render_template(
            'create_person.html',
            title="Person",
            person=person[0],
            form=form,
            option="put"
        )
    elif request.method == 'POST' and person:
        if form.validate_on_submit():
            person.first_name = form.first_name.data
            person.last_name = form.last_name.data
            person.email = form.email.data
            if form.phone.data:
                person.phone = form.phone.data
            if form.address.data:
                person.address = form.address.data
            db.session.commit()
    else:
        return 'Error loading #{id}'.format(id=id)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is not None and user.check_password(form.password.data):
            login_user(user, remember=form.remember_me.data)
            return redirect(url_for('index'))
    return render_template('login_form.html', form=form)


@app.route("/signup/", methods=["GET", "POST"])
def show_signup_form():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = UserForm()
    user = User()
    if form.validate_on_submit():
        user.username = form.username.data
        user.email = form.email.data
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()

        # Dejamos al usuario logueado
        login_user(user, remember=True)

        return redirect(url_for('list_user_view'))
    return render_template("signup_form.html", form=form)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))
