from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from test1.config.settings import SQLALCHEMY_DATABASE_URI, SQLALCHEMY_TRACK_MODIFICATIONS, SECRET_KEY

db = SQLAlchemy()
login_manager = LoginManager()


def create_app():
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)
    app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = SQLALCHEMY_TRACK_MODIFICATIONS
    app.config['SECRET_KEY'] = SECRET_KEY

    db.init_app(app)

    login_manager.init_app(app)
    login_manager.login_view = "login"

    with app.app_context():
        from test1.app import routes
        db.create_all()  # Create sql tables for our data models

        return app
