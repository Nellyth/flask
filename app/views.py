from flask_generic_views.sqlalchemy import ListView, CreateView, UpdateView, DetailView, DeleteView
from test1.app.forms import PersonFormModel, UserModelForm
from test1.app.models import Person, User
from werkzeug.security import generate_password_hash


# -------------------------------- MODEL PERSON ---------------------------------
class PersonCreateView(CreateView):
    model = Person
    form_class = PersonFormModel
    success_url = '/person_view/'
    template_name = 'create_person_view.html'


person_create = PersonCreateView.as_view('person_create')


class PersonListView(ListView):
    model = Person
    template_name = 'list_person_view.html'


person_list = PersonListView.as_view('person_list')


class PersonDetailView(DetailView):
    model = Person
    template_name = 'detail_person_view.html'


person_detail = PersonDetailView.as_view('person_detail')


class PersonUpdateView(UpdateView):
    model = Person
    form_class = PersonFormModel
    success_url = '/person_view/'
    template_name = 'update_person_view.html'


person_update = PersonUpdateView.as_view('person_update')


class PersonDeleteView(DeleteView):
    model = Person
    success_url = '/person_view/'
    template_name = 'delete_person_view.html'


person_delete = PersonDeleteView.as_view('person_delete')


# -------------------------------- MODEL USER ---------------------------------
class UseCreateView(CreateView):
    model = User
    form_class = UserModelForm
    success_url = '/user/'
    template_name = 'signup_form.html'

    def form_valid(self, form):
        form.password.data = generate_password_hash(form.password.data)
        return super().form_valid(form)


user_create = UseCreateView.as_view('user_create')


class UseCreateView(ListView):
    model = User
    template_name = 'list_user_view.html'


user_list = UseCreateView.as_view('user_list')
